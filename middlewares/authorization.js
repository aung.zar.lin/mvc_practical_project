const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
	try {
		if(req.headers.authorization) {
			let token = (req.headers.authorization).split(' ')[1];

			jwt.verify(token, process.env.PRIVATE_KEY, (err, decoded) => {
				// debugger log
				console.log('token info', decoded, null);

				(err == null) ? next() : res.json({code: 101, msg: 'permission denied'});
			});
		} else {
			res.json({code: 500, msg: 'something went wrong'});
		}
	} catch (error) {
		// debugger log
		console.log('Authorization error', error);

		res.json({code: 101, msg: 'permission denied'});
	}
}