const axios = require('axios').default;

exports.setPost = (title, body, userId) => {
	return new Promise(async (resolve, reject) => {
		try {
			let result = await axios({
				method: 'post',
				url: `${process.env.JSONPLACEHOLDER_API}/posts`,
				headers: {
					'Content-type': 'application/json; charset=UTF-8',
				},
				data: JSON.stringify({
					title,
					body,
					userId
				})
			});

			(result.status == 201) ? resolve({ code: 200, data: result.data }) : reject({ code: 500, msg: 'something went wrong'});

		} catch (error) {
			// debugger log
			console.log('setPost Error', error);
			reject({code: 500, msg: 'something went wrong'});
		}
	})
}

exports.getPost = () => {
	return new Promise(async (resolve, reject) => {
		try {
			let data = await axios.get(`${process.env.JSONPLACEHOLDER_API}/posts`);
			(data.data.length > 0) ? resolve({ code: 200, data: data.data}) : reject({code: 404, msg: 'data not found'});
 		} catch (error) {
			// debugger log
			console.log('getPost Error', error);
			reject({code: 500, msg: 'something went wrong'});
		}
	})
}

exports.putPost = (id, title, body, userId) => {
	return new Promise(async (resolve, reject) => {
		try {
			let result = await axios({
				method: 'put',
				url: `${process.env.JSONPLACEHOLDER_API}/posts/${id}`,
				headers: {
					'Content-type': 'application/json; charset=UTF-8',
				},
				data: JSON.stringify({
                    id,
					title,
					body,
					userId
				})
			});

			(result.status == 200) ? resolve({ code: 200, data: result.data }) : reject({ code: 500, msg: 'something went wrong'});

		} catch (error) {
			// debugger log
			console.log('putPost Error', error);
			reject({code: 500, msg: 'something went wrong'});
		}
	})
}

exports.deletePost = (id) => {
	return new Promise(async (resolve, reject) => {
		try {
			let result = await axios({
				method: 'delete',
				url: `${process.env.JSONPLACEHOLDER_API}/posts/${id}`
			});

			(result.status == 200) ? resolve({ code: 200, data: result.data }) : reject({ code: 500, msg: 'something went wrong'});

		} catch (error) {
			// debugger log
			console.log('putPost Error', error);
			reject({code: 500, msg: 'something went wrong'});
		}
	})
}