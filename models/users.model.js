const jwt = require('jsonwebtoken');

exports.authentication = (username, password) => {
	return new Promise(async (resolve, reject) => {
		try {

			if (username == 'admin' && password == 'admin') {
                let obj = {
                    id: 1,
                    username: 'admin'
                }

                resolve({ 
                    code: 200, 
                    data: {
                            token: jwt.sign(obj, process.env.PRIVATE_KEY, { expiresIn: '1h' })
                        }
                    }) 
            } else {
                reject({ code: 401, msg: 'unauthorized'});
            }

		} catch (error) {
			// debugger log
			console.log('setPost Error', error);
			reject({code: 500, msg: 'something went wrong'});
		}
	})
}