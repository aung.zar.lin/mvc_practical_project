// api gateway
module.exports = (express) => {
	const router = express.Router(),
		POSTS = require('../models/posts.model'),
		USERS = require('../models/users.model'),
		AUTHORIZATION = require('../middlewares/authorization'),
		USER_ID = 1;

	// username - admin , password - admin
	router.route('/users/authentication')
		.post(async (req, res) => {
			let { username, password } = req.body;

			if(!username || !password) res.json({ code: 500, msg: 'something went wrong' });

			try {
				res.json(await USERS.authentication(username, password));
			} catch (error) {
				// debugger log
				console.log('authentication error', error);
				res.json({ code: 500, msg: 'something went wrong'});
			}
		})

	router.route('/posts')
		.get(async (req, res) => res.json(await POSTS.getPost()))
		.post(AUTHORIZATION, async (req, res) => {
			let { title, body } = req.body;
			if(!title || !body) res.json({ code: 500, msg: 'something went wrong'});

			try {
				res.json(await POSTS.setPost(title, body, USER_ID));
			} catch (error) {
				// debugger log
				console.log('set controller error', error);
				res.json({ code: 500, msg: 'something went wrong'});
			}
		})
		// posts id is 0 - 100
		.put(AUTHORIZATION, async (req, res) => {
			let { id, title, body } = req.body;
			if(!id || !title || !body) res.json({ code: 500, msg: 'something went wrong' });

			try {
				res.json(await POSTS.putPost(id, title, body, USER_ID));
			} catch (error) {
				// debugger log
				console.log('put controller error', error);
				res.json({ code: 500, msg: 'something went wrong' });
			}
		})
		// posts id is 0 - 100
		.delete(AUTHORIZATION, async (req, res) => {
			let { id } = req.body;
			if(!id) res.json({ code: 500, msg: 'something went wrong' });

			try {
				res.json(await POSTS.deletePost(id));
			} catch (error) {
				// debugger log
				console.log('delete controller error', error);
				res.json(await POSTS.deletePost(id));
			}
		})

	return router;
}