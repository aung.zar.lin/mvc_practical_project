var express = require('express'),
    app 	= express(),
    rateLimit = require('express-rate-limit'),
    apiController = require('./controllers/api.controller')(express);
  
require('dotenv').config();

const rateLimiter = rateLimit({
	windowMs: 1000, // 1 seconds
	max: 5, // Limit each IP to 5 requests per `window` (here, per 1 seconds)
})

app.use(express.json({limit: '10mb'})) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use('/api', rateLimiter, apiController)

app.listen(process.env.PORT, () => console.log('Listening on port ' + process.env.PORT));